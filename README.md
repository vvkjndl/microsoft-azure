# Microsoft Azure

## blob-search.ps1

This script will help you in searching for blobs in Azure Blob Storage.

- Configure $subdomain value in script.
- Clone this repository on Desktop and Import the script blob-search.ps1 as a PowerShell module.

`
Import-Module -FullyQualifiedName "$home\destop\microsoft-azure\blob-search.ps1" -DisableNameChecking -Force
`

- You can then use below function to search blobs.

```
AzureBlob-Search "*ABC*" "Thu*21"       Shows the list of apps matching the name and date pattern from blob storage.
  -All                                  Shows the complete listing of apps instead of matching the search pattern.
```

## config.config

Declare config for devops.ps1 here.


## devops.ps1

Clone this repository on Desktop and Import the script devops.ps1 as a PowerShell module.

`
Import-Module -FullyQualifiedName "$home\destop\microsoft-azure\devops.ps1" -DisableNameChecking -Force
`

- This script handles the installation and upgrade of Azure CLI including its devops extension.
- This script can be used to create Azure build pipelines for BC apps.
- This script will modify settings.json as well for BC pipeline executions.
- Pipeline scripts are sourced from BC HelloWorld repo.

`https://dev.azure.com/businesscentralapps/HelloWorld/_git/HelloWorld`

```
DevOps-AddModules               Installs/Upgrades Azure CLI and devops extensions.
DevOps-SetConfig                Loads Azure CLI configuration.
DevOps-ShowConfig               Shows Azure CLI configuration.

DevOps-CreateBuildPipeline      Creates DevOps build pipeline.
  -Name "ABC XYZ"               Specify name for the pipeline.
  -Current                      Create pipeline for Current.
  -CI                           Create pipeline for CI as well.
  -Cloud                        Create pipeline for Cloud as well.
  -NextMinor                    Create NextMinor pipeline as well.
  -NextMajor                    Create NextMajor pipeline as well.
  -RepositoryType "tfsgit"      Specify repository type for the pipeline. "tfsgit" is the default value.
  -RepositoryName "YIBASE"      Specify name of the repository for the pipeline.
  -RepositoryBranch "test"      Specify branch of the repository.
  -PoolName "Default"           Specify Agent Pool to be used for the pipeline runs.

DevOps-Help                     Shows this help page.
```
