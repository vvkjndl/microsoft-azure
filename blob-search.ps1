﻿# enforce tls12 usage
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12


function global:AzureBlob-Search {

    param (

        [Parameter(Mandatory = $False)]
        [string] $Name,
        [Parameter(Mandatory = $False)]
        [string] $Modified,
        [Parameter(Mandatory = $False)]
        [switch] $All

    )

    if (!($Name) -and !($All)) {
        Write-Warning -Message "You must specify an app name or use the -All switch."
        return
    }

    # obtain blob list in xml using azure rest apis
    $subdomain = "ABCXYZ"
    $uridomain = "https://$subdomain.blob.core.windows.net/"
    $urirequest = "apps?restype=container&comp=list&"
    $urisas = "sastoken"
    $uri = $uridomain + $urirequest + $urisas
    $blobxml = (Invoke-RestMethod -Uri $uri -Method Get -ContentType 'application/xml' -UseBasicParsing).Replace('ï»¿', '')

    # convert xml to table format
    $blobxml = ([xml]($blobxml)).EnumerationResults.Blobs.Blob | Select-Object -Property Name, Properties
    $blobtable = @()
    $blobxml | ForEach-Object {
        $blobtable += New-Object -TypeName psobject -Property @{
            Name     = $_.Name
            Modified = $_.Properties.'Last-Modified'
            Created  = $_.Properties.'Creation-Time'
            Length   = $_.Properties.'Content-Length'
            MD5      = $_.Properties.'Content-MD5'
        }
    }

    if ($All) {
        $blobtable | Sort-Object -Property Name | Format-Table -AutoSize -Property Name, Modified, Created, Length, MD5 -Force
    }
    else {
        if ($Modified) {
            $blobtable | Where-Object {$_.Name -like "*$Name*"} | Where-Object {$_.Modified -like "*$Modified*"} | Sort-Object -Property Name | Format-Table -AutoSize -Property Name, Modified, Created, Length, MD5 -Force
        }
        else {
            $blobtable | Where-Object {$_.Name -like "*$Name*"} | Sort-Object -Property Name | Format-Table -AutoSize -Property Name, Modified, Created, Length, MD5 -Force
        }
    }

}


# function to show help
function global:AzureBlob-Help {

    Write-Host -NoNewline -Object "`n"

    Write-Host -NoNewline -ForegroundColor Yellow -Object "AzureBlob-Search"
    Write-Host -NoNewline -Object " `"*ABC*`" `"Thu*21`"`t"
    Write-Host -Object "Shows the list of apps matching the name and date pattern from blob storage."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -All`t`t`t`t`t"
    Write-Host -Object "Shows the complete listing of apps instead of matching the search pattern."

    Write-Host -NoNewline -Object "`n"

}


# show help on launch
AzureBlob-Help
