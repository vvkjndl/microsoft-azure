# enforce tls12 usage
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12

# this function handles the installation of az cli modules/extensions
function global:DevOps-AddModules {

    # check if az cli module is already installed
    if (Get-Command -Name "az" -ErrorAction SilentlyContinue) {

        Write-Host -Object "`nAzure CLI module already installed..."

        # upgrade existing az cli module
        Write-Host -Object "Checking updates for Azure CLI module..."
        Start-Process -FilePath "cmd.exe" -ArgumentList '/C az upgrade --all false --verbose && pause' -Verb RunAs -Wait

        # upgrade existing az cli devops extension
        Write-Host -Object "Checking updates for Azure CLI DevOps extension...`n"
        Start-Process -FilePath "cmd.exe" -ArgumentList '/C az extension update --name azure-devops --verbose && pause' -Verb RunAs -Wait

    }
    else {

        # source file url and download destination
        $url = "https://aka.ms/installazurecliwindows"
        $filename = "$home\AzureCLI.msi"

        # remove previously existing file
        Remove-Item -Path $filename -Force -ErrorAction SilentlyContinue

        # obtain file download size (kB)
        $webrequest = Invoke-WebRequest -Uri $url -Method Head
        $filesizetotal = $webrequest.Headers.'Content-Length'

        # start download job in background
        Write-Host -Object "`nDownloading the MSI installer for Azure CLI module..."
        $downloadjob = Start-Job -Name "Download-AzureCLI" -ScriptBlock {Invoke-WebRequest -Uri $args[0] -OutFile $args[1] -UseBasicParsing} -ArgumentList "$url", "$filename"

        # show download progress until completed
        while ($downloadjob.State -ne "Completed") {

            # wait for file to be created
            while (!(Get-ChildItem -Path $filename -ErrorAction SilentlyContinue)) {
                Start-Sleep -Seconds 1
            }

            # calculate current download progress in %
            $currentprogress = [math]::Round((((Get-Item -Path $filename).Length) / $filesizetotal) * 100)
            Write-Host -Object "$currentprogress% completed..."
            Start-Sleep -Seconds 3

        }

        # run the installer for azure cli
        Write-Host -Object "Download completed, running installer in the background..."
        $install = Start-Process -FilePath "msiexec.exe" -Wait -ArgumentList "/package $filename /quiet" -Verb RunAs
        Write-Host -Object "Installer process exited..."

        # remove the file after installation
        Write-Host -Object "Removing the downloaded installer..."
        Remove-Item -Path "$filename" -Force

        # refresh powershell environment variables
        Write-Host -Object "Refreshing PowerShell environment variables..."
        $env:Path = [System.Environment]::GetEnvironmentVariable("Path", "Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path", "User") 

        # install azure cli devops extension
        Write-Host -Object "Installing Azure CLI DevOps extension...`n"
        Start-Process -FilePath "cmd.exe" -Wait -ArgumentList '/C az extension add --name azure-devops --verbose' -Verb RunAs

        # run initial az cli configuration
        DevOps-SetConfig

    }

}

# this function will handle configuration file changes for az cli
function global:DevOps-SetConfig {

    # read configuration file values from config.config
    Write-Host -Object "`nReading configuration values from config.config..."
    Get-Content -Path "$home\Desktop\microsoft-azure\config.config" | ForEach-Object `
        -Begin {$h = @{}} `
        -Process {
        $k = [regex]::split($_, '=')
        if (($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) {
            $h.Add($k[0], $k[1])
        }
        else {
        }
    }

    # abort if az module installation not found
    if (!(Get-Command -Name "az" -ErrorAction SilentlyContinue)) {
        Write-Warning -Message "Azure CLI module installation not found, function $($MyInvocation.MyCommand.Name) failed."
        return
    }

    Write-Host -Object "Setting DevOps configuration variables...`n"

    # devops org and project details
    $global:devopsproject = $h.DevOpsproject
    $global:devopsorg = "https://dev.azure.com/" + $($h.DevOpsOrg)

    # set devops configuration and token as environment variable
    $ENV:AZURE_DEVOPS_EXT_PAT = $h.DevOpsToken
    $global:DevOpsToken = $h.DevOpsToken
    az devops configure --defaults project=$devopsproject organization=$devopsorg

    # git user and repo path declaration
    $user = $h.Username.Split("@") | Select-Object -First 1
    $domain = $Username.Split("@") | Select-Object -First 2 -Skip 1
    $global:gitusr = "https://" + $user + "%40" + $domain + ":"
    $global:repopath = "@dev.azure.com/" + $($h.DevOpsOrg) + "/" + $devopsproject + "/_git/"

    # disable cli preview warnings
    az config set core.only_show_errors=yes

    # by default, an extension command that prompts dynamic install will not continue to run.
    az config set extension.run_after_dynamic_install=yes

    # when you run an extension command that is not installed, the azure cli can recognize the command you run, and automatically install the extension for you.
    az config set extension.use_dynamic_install=yes

    # enable logging
    az config set logging.enable_log_file=yes

    # disable az cli auto-upgrade
    az config set auto-upgrade.enable=no

    # disable spyware (benefit of doubt nevertheless)
    az config set core.collect_telemetry=no

    # turn off confirmation prompts
    az config set core.disable_confirm_prompt=yes

    # table output by default (default is none)
    az config set core.output=table

}

# shows configuration details of az cli
function global:DevOps-ShowConfig {

    # abort if az module installation not found
    if (!(Get-Command -Name "az" -ErrorAction SilentlyContinue)) {
        Write-Warning -Message "Azure CLI module installation not found, function $($MyInvocation.MyCommand.Name) failed."
        return
    }

    # display az cli configuration on console
    Write-Host -NoNewline -Object "`n"
    @("core", "extension", "logging", "auto-upgrade") | ForEach-Object {
        Write-Host -ForegroundColor Yellow -Object "Showing Azure CLI configuration details for $_...`n"
        az config get $_
        Write-Host -NoNewline -Object "`n"
    }

    # display devops extension configuration on console
    Write-Host -ForegroundColor Yellow -Object "`nShowing configuration details for the DevOps extension...`n"
    az devops configure --list
    Write-Host -NoNewline -Object "`n"

}

function global:DevOps-CreateBuildPipeline {

    param (

        [Parameter(Mandatory = $True)]
        [string] $Name,

        [Parameter(Mandatory = $False)]
        [switch] $Current,

        [Parameter(Mandatory = $False)]
        [switch] $CI,

        [Parameter(Mandatory = $False)]
        [switch] $Cloud,

        [Parameter(Mandatory = $False)]
        [switch] $NextMinor,

        [Parameter(Mandatory = $False)]
        [switch] $NextMajor,

        [Parameter(Mandatory = $False)]
        [string] $RepositoryType = "tfsgit",

        [Parameter(Mandatory = $True)]
        [string] $RepositoryName,

        [Parameter(Mandatory = $True)]
        [string] $RepositoryBranch,

        [Parameter(Mandatory = $True)]
        [string] $PoolName

    )

    # abort if az module installation not found
    if (!(Get-Command -Name "az" -ErrorAction SilentlyContinue)) {
        Write-Warning -Message "Azure CLI module installation not found, function $($MyInvocation.MyCommand.Name) failed."
        return
    }

    # abort if no pipeline specified to create
    if (!($Current) -and !($CI) -and !($Cloud) -and !($NextMinor) -and !($NextMajor)) {
        Write-Warning -Message "You must specify at least one pipeline to create."
        return
    }

    # obtain queue id for the specified pool
    $queueid = ((az pipelines queue list --output json | ConvertFrom-Json) | Where-Object {$_.name -eq $PoolName}).id

    # sometimes az queue list returns incomplete listing and queue id for the specified pool can't be determined
    if ($null -eq $queueid) {
        Write-Host -ForegroundColor Yellow -Object "Failed to determine queue id for the pool $PoolName, try again.`n"
        return
    }

    # obtain pipeline listing
    $pipelinelist = az pipelines list --output json | ConvertFrom-Json

    # create pipeline for current using current.yml
    if ($Current) {
        if ($pipelinelist.name -contains "$Name Current") {
            Write-Host -ForegroundColor Yellow -Object "`nPipeline already exists with that name, here are the details...`n"
            az pipelines show --name "$Name Current"
            Write-Host -NoNewline -Object "`n"
        }
        else {
            Write-Host -ForegroundColor Yellow -Object "`nCreating Pipeline for Current...`n"
            az pipelines create --name "$Name Current" --repository-type $RepositoryType --repository $RepositoryName --branch $RepositoryBranch --yaml-path "scripts\Current.yml" --queue-id $queueid --skip-first-run
            Write-Host -NoNewline -Object "`n"
        }

    }

    if ($CI) {
        if ($pipelinelist.name -contains "$Name CI") {
            Write-Host -ForegroundColor Yellow -Object "`nPipeline already exists with that name, here are the details...`n"
            az pipelines show --name "$Name CI"
            Write-Host -NoNewline -Object "`n"
        }
        else {
            Write-Host -ForegroundColor Yellow -Object "`nCreating Pipeline for CI...`n"
            az pipelines create --name "$Name CI" --repository-type $RepositoryType --repository $RepositoryName --branch $RepositoryBranch --yaml-path "scripts\CI.yml" --queue-id $queueid --skip-first-run
            Write-Host -NoNewline -Object "`n"
        }

    }

    # create cloud pipeline using cloud.yml, if parameter is specified
    if ($Cloud) {
        if ($pipelinelist.name -contains "$Name Cloud") {
            Write-Host -ForegroundColor Yellow -Object "`nPipeline already exists with that name, here are the details...`n"
            az pipelines show --name "$Name Cloud"
            Write-Host -NoNewline -Object "`n"
        }
        else {
            Write-Host -ForegroundColor Yellow -Object "`nCreating Pipeline for Cloud...`n"
            az pipelines create --name "$Name Cloud"--repository-type $RepositoryType --repository $RepositoryName --branch $RepositoryBranch --yaml-path "scripts\Cloud.yml" --queue-id $queueid --skip-first-run
            Write-Host -NoNewline -Object "`n"
        }
    }

    # create next minor pipeline using nextminor.yml, if parameter is specified
    if ($NextMinor) {
        if ($pipelinelist.name -contains "$Name Next Minor") {
            Write-Host -ForegroundColor Yellow -Object "`nPipeline already exists with that name, here are the details...`n"
            az pipelines show --name "$Name Next Minor"
            Write-Host -NoNewline -Object "`n"
        }
        else {
            Write-Host -ForegroundColor Yellow -Object "`nCreating Pipeline for Next Minor...`n"
            az pipelines create --name "$Name Next Minor"--repository-type $RepositoryType --repository $RepositoryName --branch $RepositoryBranch --yaml-path "scripts\NextMinor.yml" --queue-id $queueid --skip-first-run
            Write-Host -NoNewline -Object "`n"
        }
    }

    # create next major pipeline using nextmajor.yml, if parameter is specified
    if ($NextMajor) {
        if ($pipelinelist.name -contains "$Name Next Major") {
            Write-Host -ForegroundColor Yellow -Object "`nPipeline already exists with that name, here are the details...`n"
            az pipelines show --name "$Name Next Major"
            Write-Host -NoNewline -Object "`n"
        }
        else {
            Write-Host -ForegroundColor Yellow -Object "`nCreating Pipeline for Next Major...`n"
            az pipelines create --name "$Name Next Major" --repository-type $RepositoryType --repository $RepositoryName --branch $RepositoryBranch --yaml-path "scripts\NextMajor.yml" --queue-id $queueid --skip-first-run
            Write-Host -NoNewline -Object "`n"
        }
    }
    Write-Host -NoNewline -Object "`n"

    # add pipeline scripts
    $workdir = "C:\ProgramData\Azure DevOps"
    New-Item -Path $workdir -ItemType Directory -Force | Out-Null
    Set-Location -Path $workdir
    $repo = "HelloWorld"
    Write-host -Object "Checking out $repo repository..."
    if (Test-Path -Path $workdir\$repo -PathType Container) {
        Set-Location -Path $workdir\$repo
        git pull https://businesscentralapps@dev.azure.com/businesscentralapps/HelloWorld/_git/HelloWorld
    }
    else {
        git clone https://businesscentralapps@dev.azure.com/businesscentralapps/HelloWorld/_git/HelloWorld
    }
    Set-Location -Path $workdir

    # get the app repo
    $repo = $Name
    Write-host -Object "Checking out $repo repository..."
    if (Test-Path -Path $workdir\$repo -PathType Container) {
        Set-Location -Path $workdir\$repo
        git switch $RepositoryBranch
        git pull ($gitusr + $DevOpsToken + $repopath + $repo)
    }
    else {
        git clone ($gitusr + $DevOpsToken + $repopath + $repo)
        Set-Location -Path $workdir\$repo
        git switch $RepositoryBranch
    }
    Set-Location -Path $workdir

    # create scripts folder
    Write-host -ForegroundColor Yellow -Object "Adding scripts folder to repo $Name..."
    if (Test-Path -Path "$workdir\$Name\scripts" -PathType Container ) {
        Write-host -ForegroundColor Yellow -Object "Scripts folder already exists in repo $Name..."
    }
    else {
        New-Item -Path $workdir\$Name -Name "scripts" -ItemType Directory -Force | Out-Null
    }

    # copy scripts folder files
    Get-ChildItem -Path "$workdir\HelloWorld\scripts" -File | ForEach-Object {
        $filename = $_
        if (!(Test-Path -Path "$workdir\$Name\scripts\$($filename.Name)" -PathType Leaf)) {
            Copy-Item -Path $filename.FullName -Destination "$workdir\$Name\scripts" -Force | Out-Null
        }
    }

    # update settings.json file
    Write-host -Object "Updating settings.json file..."
    $settingsjson = Get-Content -Path "$workdir\$Name\scripts\settings.json" -Raw | ConvertFrom-Json
    $settingsjson.name = $Name
    $settingsjson | ConvertTo-Json -Depth 32 | Set-Content -Path "$workdir\$Name\scripts\settings.json" -NoNewline -Force

    # update blob dependencies in settings.json file
    if (Test-Path -Path "$workdir\$Name\test\app.json" -PathType Leaf) {
        $settingsjson = Get-Content -Path "$workdir\$Name\scripts\settings.json" -Raw | ConvertFrom-Json
        $appjson = Get-Content -Path "$workdir\$Name\test\app.json" -Raw | ConvertFrom-Json
        $dependencies = (($appjson.dependencies | Where-Object {$_.publisher -like "inhouse llc*"}).name -replace " - .*", "") -join ','
        $settingsjson.installApps = $dependencies
        $settingsjson | ConvertTo-Json -Depth 32 | Set-Content -Path "$workdir\$Name\scripts\settings.json" -NoNewline -Force
    }
    else {
        $settingsjson = Get-Content -Path "$workdir\$Name\scripts\settings.json" -Raw | ConvertFrom-Json
        $appjson = Get-Content -Path "$workdir\$Name\app\app.json" -Raw | ConvertFrom-Json
        $dependencies = (($appjson.dependencies | Where-Object {$_.publisher -like "inhouse llc*"}).name -replace " - .*", "") -join ','
        $settingsjson.installApps = $dependencies
        $settingsjson | ConvertTo-Json -Depth 32 | Set-Content -Path "$workdir\$Name\scripts\settings.json" -NoNewline -Force
    }

    # push scripts folder to the repo
    Set-Location -Path $workdir\$Name
    git pull --all
    git add ./scripts
    git commit -m "added scripts folder"
    git push origin $RepositoryBranch
    Set-Location -Path $workdir

}


# this function will display usage help on console
function global:DevOps-Help {

    Write-Host -NoNewline -Object "`n"

    Write-Host -NoNewline -ForegroundColor Yellow -Object "DevOps-AddModules`t`t"
    Write-Host -Object "Installs/Upgrades Azure CLI and devops extensions."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "DevOps-SetConfig`t`t"
    Write-Host -Object "Loads Azure CLI configuration."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "DevOps-ShowConfig`t`t"
    Write-Host -Object "Shows Azure CLI configuration."

    Write-Host -NoNewline -Object "`n"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "DevOps-CreateBuildPipeline`t"
    Write-Host -Object "Creates DevOps build pipeline."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -Name"
    Write-Host -NoNewline -Object " `"ABC XYZ`"`t`t"
    Write-Host -Object "Specify name for the pipeline."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -Current`t`t`t"
    Write-Host -Object "Create pipeline for Current."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -CI`t`t`t`t"
    Write-Host -Object "Create pipeline for CI as well."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -Cloud`t`t`t"
    Write-Host -Object "Create pipeline for Cloud as well."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -NextMinor`t`t`t"
    Write-Host -Object "Create NextMinor pipeline as well."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -NextMajor`t`t`t"
    Write-Host -Object "Create NextMajor pipeline as well."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -RepositoryType"
    Write-Host -NoNewline -Object " `"tfsgit`"`t"
    Write-Host -Object "Specify repository type for the pipeline. `"tfsgit`" is the default value."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -RepositoryName"
    Write-Host -NoNewline -Object " `"YIBASE`"`t"
    Write-Host -Object "Specify name of the repository for the pipeline."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -RepositoryBranch"
    Write-Host -NoNewline -Object " `"test`"`t"
    Write-Host -Object "Specify branch of the repository."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -PoolName"
    Write-Host -NoNewline -Object " `"Default`"`t`t"
    Write-Host -Object "Specify Agent Pool to be used for the pipeline runs."

    Write-Host -NoNewline -Object "`n"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "DevOps-Help`t`t`t"
    Write-Host -Object "Shows this help page."

    Write-Host -NoNewline -Object "`n"

}

<# .psm1 code

    # list of functions to export
    Export-ModuleMember -Function DevOps-AddModules, DevOps-SetConfig, DevOps-ShowConfig, DevOps-CreateBuildPipeline, DevOps-Help

#>

# show help on first run
DevOps-Help

# apply configuration on startup
DevOps-SetConfig
